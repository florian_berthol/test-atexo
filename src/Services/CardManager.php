<?php
namespace App\Services;


use App\Entity\CardColor;
use App\Entity\CardValue;
use Doctrine\ORM\EntityManagerInterface;

class CardManager
{
    /** @var int  */
    const ORDER_MODIFIER = 100;

    /** @var EntityManagerInterface  */
    private $manager;

    /**
     * CardManager constructor.
     *
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Get Colors from database
     *
     * @return CardColor[]|null
     */
    protected function getColors()
    {
        return $this->manager
            ->getRepository(CardColor::class)
            ->findAll();
    }

    /**
     * Get cards values from database
     *
     * @return CardValue[]|null
     */
    protected function getValues()
    {
        return $this->manager
            ->getRepository(CardValue::class)
            ->findAll();
    }

    /**
     * Construct the card game
     *
     * @return array
     * @throws \Exception
     */
    protected function getCards() : array
    {
        $list = [];
        $colors = $this->getColors();
        dump ($colors);
        $values = $this->getValues();

        if (empty($colors) || empty($values)) {
            throw new \Exception('Can\'t find cards in database');
        }

        foreach ($colors as $color) {
            foreach ($values as $value) {
                $list[] = [
                    'color' => $color->getName(),
                    'name' => $value->getName(),
                    'value' => $value->getValue(),
                    'order' => $value->getValue() + ($color->getOrder() * self::ORDER_MODIFIER)
                ];
            }
        }

        return $list;
    }

    /**
     * Sort the card game
     *
     * @param array $cards
     * @return array
     */
    public function sortCards(array $cards) : array
    {
        usort($cards, function ($a, $b) {
            if ($a['order'] > $b['order']) {
                return true;
            }

            return false;
        });

        return $cards;
    }

    /**
     * Distribute the cards
     *
     * @param int $nb
     * @return array
     * @throws \Exception
     */
    public function drawCards(int $nb) : array
    {
        $cards = $this->getCards();
        shuffle($cards);

        return array_slice($cards, 0, $nb);
    }
}
