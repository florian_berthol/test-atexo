<?php

namespace App\DataFixtures;

use App\Entity\CardColor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ColorFixtures extends Fixture
{
    const COLOR_LIST = [
        ['name' => 'clubs', 'order' => 1],
        ['name' => 'diamonds', 'order' => 2],
        ['name' => 'hearts', 'order' => 4],
        ['name' => 'spades', 'order' => 3]
    ];

    public function load(ObjectManager $manager)
    {
        foreach (self::COLOR_LIST as $color) {
            $newColor = new CardColor();
            $newColor->setName($color['name'])
                ->setOrder($color['order']);
            $manager->persist($newColor);
        }

        $manager->flush();
    }
}
