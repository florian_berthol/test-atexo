<?php

namespace App\DataFixtures;

use App\Entity\CardValue;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ValueFixtures extends Fixture
{
    const VALUE_LIST = [
        ['name' => 'ace', 'value' => 1],
        ['name' => 'two', 'value' => 2],
        ['name' => 'three', 'value' => 3],
        ['name' => 'four', 'value' => 4],
        ['name' => 'five', 'value' => 5],
        ['name' => 'six', 'value' => 6],
        ['name' => 'seven', 'value' => 7],
        ['name' => 'eight', 'value' => 8],
        ['name' => 'nine', 'value' => 9],
        ['name' => 'ten', 'value' => 10],
        ['name' => 'jack', 'value' => 11],
        ['name' => 'queen', 'value' => 12],
        ['name' => 'king', 'value' => 13],
    ];

    public function load(ObjectManager $manager)
    {
        foreach (self::VALUE_LIST as $value) {
            $newValue = new CardValue();
            $newValue->setName($value['name']);
            $newValue->setValue($value['value']);
            $manager->persist($newValue);
        }

        $manager->flush();
    }
}
