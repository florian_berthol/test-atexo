<?php

namespace App\Controller;

use App\Services\CardManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CardController extends AbstractController
{
    /**
     * @Route("/", name="default")
     * @Route("/{_locale}", name="index", requirements={"_locale":"fr|en"})
     */
    public function index(CardManager $cardManager)
    {
        $cardList = $cardManager->drawCards(10);

        return $this->render('card/index.html.twig', [
            'card_list' => $cardList,
            'ordered_card_list' => $cardManager->sortCards($cardList)
        ]);
    }
}
