<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use App\Services\CardManager;

class CardManagerTest extends TestCase
{
    private $cardManager;

    protected function setUp()
    {
        $this->cardManager = $this->getMockBuilder(CardManager::class)
            ->setConstructorArgs([$this->createMock(EntityManagerInterface::class)])
            ->setMethods(['getColors', 'getValues'])
            ->getMock();
    }

    public function testGetCards()
    {
        $this->cardManager
            ->expects($this->once())
            ->method('getColors')
            ->willReturn([
                new class {
                    function getName() {
                        return 'spades';
                    }
                    function getOrder() {
                        return '3';
                    }
                }
            ]);

        $this->cardManager
            ->expects($this->once())
            ->method('getValues')
            ->willReturn([
                new class {
                    function getName() {
                        return 'ace';
                    }
                    function getValue() {
                        return 1;
                    }
                }
            ]);

        $cards = $this->cardManager->drawCards(1);
        $this->assertEquals($cards[0]['color'], 'spades');
        $this->assertEquals($cards[0]['name'], 'ace');
        $this->assertEquals($cards[0]['value'], 1);
        $this->assertEquals($cards[0]['order'], 301);
    }

    public function testGetCardsNoOk()
    {
        $this->cardManager
            ->expects($this->once())
            ->method('getColors')
            ->willReturn(null);

        $this->cardManager
            ->expects($this->once())
            ->method('getValues')
            ->willReturn(null);

        try {
            $cards = $this->cardManager->drawCards(1);
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), 'Can\'t find cards in database');
        }
    }

    public function testDrawCards()
    {
        $cardManager = $this->getMockBuilder(CardManager::class)
            ->setConstructorArgs([$this->createMock(EntityManagerInterface::class)])
            ->setMethods(['getCards'])
            ->getMock();
        $cardManager
            ->expects($this->once())
            ->method('getCards')
            ->willReturn([[],[],[],[],[]]);

        $cards = $cardManager->drawCards(2);
        $this->assertEquals(count($cards), 2);
    }

    public function testSortCard()
    {
        $cards = [[
                "color" => "spades",
                "name" => "three",
                "value" => 3,
                "order" => 303
            ], [
                "color" => "hearts",
                "name" => "two",
                "value" => 2,
                "order" => 402,
            ], [
                "color" => "spades",
                "name" => "ace",
                "value" => 1,
                "order" => 301,
            ]
        ];

        $cards = $this->cardManager->sortCards($cards);
        $this->assertEquals($cards[0]['color'], 'spades');
        $this->assertEquals($cards[0]['name'], 'ace');
        $this->assertEquals($cards[0]['value'], 1);
        $this->assertEquals($cards[0]['order'], 301);

        $this->assertEquals($cards[1]['color'], 'spades');
        $this->assertEquals($cards[1]['name'], 'three');
        $this->assertEquals($cards[1]['value'], 3);
        $this->assertEquals($cards[1]['order'], 303);

        $this->assertEquals($cards[2]['color'], 'hearts');
        $this->assertEquals($cards[2]['name'], 'two');
        $this->assertEquals($cards[2]['value'], 2);
        $this->assertEquals($cards[2]['order'], 402);
    }
}
